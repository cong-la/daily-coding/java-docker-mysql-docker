package com.simsoft.api.sample01.repository;

import com.simsoft.api.sample01.model.SchoolInfo;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.ArrayList;

public interface SchoolInfoRepository extends JpaRepository<SchoolInfo, Integer> {

    public ArrayList<SchoolInfo> findAll();

    public SchoolInfo findById(int id);

    public SchoolInfo save(SchoolInfo schoolInfo);

}
