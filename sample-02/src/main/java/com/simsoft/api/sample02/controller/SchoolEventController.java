package com.simsoft.api.sample02.controller;

import com.simsoft.api.sample02.model.SchoolEvent;
import com.simsoft.api.sample02.repository.SchoolEventRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.ArrayList;

@RestController
@RequestMapping(path = "/event")
public class SchoolEventController {

    @Autowired
    private SchoolEventRepository schoolEventRepository;

    @GetMapping()
    public ArrayList<SchoolEvent> findAll() {
        return this.schoolEventRepository.findAll();
    }

    @GetMapping(path = "/{id}")
    public SchoolEvent findById(@PathVariable int id) {
        return this.schoolEventRepository.findByEventId(id);
    }

    @PostMapping()
    public SchoolEvent save(@RequestBody SchoolEvent schoolEvent) {
        return this.schoolEventRepository.save(schoolEvent);
    }
}
